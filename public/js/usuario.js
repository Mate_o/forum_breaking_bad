import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-auth.js";
import { getDatabase, set, ref } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyA7Ewdjg47WUWBACrtY6RdeYbg42eSwhac",
    authDomain: "foro-e8aae.firebaseapp.com",
    projectId: "foro-e8aae",
    storageBucket: "foro-e8aae.appspot.com",
    messagingSenderId: "192321753368",
    appId: "1:192321753368:web:c74d978dcb62370bb2ce6d"
    };

const app = initializeApp(firebaseConfig);
const database = getDatabase(app)

let correoRegisterRef = document.getElementById("input-email-r")
let passRegisterRef = document.getElementById("input-pass-r")
let botonRegisterRef = document.getElementById("boton-registrarse")

let correoIniciarRef = document.getElementById("input-email-iniciar")
let passIniciarRef = document.getElementById("input-pass-iniciar")
let botonIniciarRef = document.getElementById("boton-iniciar")

let nombreRef = document.getElementById("input-nombre")
let ciudadRef = document.getElementById("input-ciudad")
const auth = getAuth();

botonRegisterRef.addEventListener("click", sigIn)
botonIniciarRef.addEventListener("click", logIn)

function sigIn(){
    if((correoRegisterRef.value != '') && (passRegisterRef != '')){

        createUserWithEmailAndPassword(auth, correoRegisterRef.value, passRegisterRef.value)
        .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          console.log("Usuario creado correctamente")
          // ...
          let nombre = nombreRef.value
          let ciudad = ciudadRef.value

          set(ref(database, 'usuarios/' + user.uid), {

            Nombre: nombre,
            Ciudad: ciudad
          }).then(() => {
            window.location.href = "../index.html"
          })   
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log("Codigo de error:" + errorCode + "Mensaje de error: " + errorMessage)
          // ..
        });
    }
    else{
        alert("NO")
    }
}

function logIn(){
    if((correoIniciarRef !='') && (passIniciarRef != '')){
        signInWithEmailAndPassword(auth, correoIniciarRef.value, passIniciarRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            window.location.href = "../index.html"
            // ...
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Codigo de error: " + errorCode + " Mensaje: " + errorMessage)
        });
    }
    else{
        alert("NO")
    }
}

