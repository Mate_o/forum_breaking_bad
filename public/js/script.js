import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js";
import { getAuth, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-auth.js";
import { getDatabase, ref, set, onValue, push, child, query, orderByKey } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyA7Ewdjg47WUWBACrtY6RdeYbg42eSwhac",
    authDomain: "foro-e8aae.firebaseapp.com",
    projectId: "foro-e8aae",
    storageBucket: "foro-e8aae.appspot.com",
    messagingSenderId: "192321753368",
    appId: "1:192321753368:web:c74d978dcb62370bb2ce6d"
    };

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();

let textRef = document.getElementById("input-text")
let botonRef = document.getElementById("boton-comentar")
let comentarioRef = document.getElementById("comentario")
let cerrarsesionRef = document.getElementById("cerrarsesionBoton")

var mail;
cerrarsesionRef.addEventListener("click", cerrarSesion)

function cerrarSesion(){
  signOut(auth).then(() => {
    console.log("Sesion cerrada")
  }).catch((error) => {
    // Error ha aparecer.
  });
}
onAuthStateChanged(auth, (user) => {
    if (user) {
      const uid = user.uid;
      console.log("Usuario actual: " + uid)
      const correo = user.email
      console.log(correo)
      botonRef.addEventListener("click", coment)
      // ...
      mail = correo
    } else {
      console.log("Ningún usuario")
      botonRef.addEventListener("click", iniciarsesion)
    }
  });

function iniciarsesion(){
    window.location.href = "../pages/usuario.html"
}

function coment(){
    let comentario = textRef.value

    push(ref(database, 'comentarios/'), {
        msg: comentario
      });
    
      textRef.value = ''
      console.log("Comentario realizado")
      window.location.reload()
}

const queryMensajes = query(ref(database, 'comentarios/'),
orderByKey('msg'))

onValue(queryMensajes, (snapshot) => {
    const lectura = snapshot.val();
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val().msg
      console.log(childData)

    comentarioRef.innerHTML +=`
   <div class="card mb-3 cardUsuario" style="max-width: 540px; margin-top: 2rem;">
    <div class="row g-0 coment">
      <div class="col-md-4">
        <img src="./media/heisenberg.png" class="img-fluid rounded-start" alt="Ups...">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">${mail}</h5>
          <p class="card-text">${childSnapshot.val().msg}</p>
        </div>
      </div>
    </div>
  </div>
`
  })
})